/**
* @Auther cy
* @Date:2021/8/11 14:43
 */

package tool

import (
	"fmt"
	"testing"
	"time"
)

func TestNacos(t *testing.T) {
	client := NewNacosClient()
	nacosConfig := NacosConf{
		Address:     []string{""},
		NamespaceID: "8c651981-22d1-4100-82d8-8f507bd8a2f0",
		ServiceIP:   "0.0.0.0",
		ServicePort: 3031,
	}
	client.NacosRegister("test1", nacosConfig)
	defer client.DeregisterRegister("test1", nacosConfig)
	for {
		fmt.Println("------------------")
		fmt.Println(client.GetIpPortByName("test"))
		time.Sleep(5 * time.Second)
	}
}
