package tool

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"
	"fmt"
)

func AESDecrypt(key string, val string, iv string) (string, error) {
	origData, err := base64.RawURLEncoding.DecodeString(val)
	b, err := decrypt(key, origData, iv)
	return string(b), err
}
func AESEncrypt(key string, val string, iv string) (string, error) {
	origData := []byte(val)
	crypted, err := encrypt(key, origData, iv)
	if err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(crypted), nil
}
func getKeyBytes(key string) []byte {
	keyBytes := []byte(key)
	switch l := len(keyBytes); {
	case l < 16:
		keyBytes = append(keyBytes, make([]byte, 16-l)...)
	case l > 16:
		keyBytes = keyBytes[:16]
	}
	return keyBytes
}

// body 请求体参数详见api
func decrypt(key string, crypted []byte, iv string) (b []byte, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.New(fmt.Sprintf("%s", e))
			return
		}
	}()
	keyBytes := getKeyBytes(key)
	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return nil, err
	}
	blockMode := cipher.NewCBCDecrypter(block, getKeyBytes(iv))
	origData := make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	return origData, nil
}
func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

/*
key iv dhccplat  val abcdefg123456
URLEncoding X8N3ojrtNd_WzVTVCbSNaw==
StdEncoding X8N3ojrtNd/WzVTVCbSNaw==
RawURLEncoding X8N3ojrtNd_WzVTVCbSNaw
RawStdEncoding X8N3ojrtNd/WzVTVCbSNaw
*/

func encrypt(key string, origData []byte, iv string) (b []byte, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = errors.New(fmt.Sprintf("%s", e))
			return
		}
	}()
	keyBytes := getKeyBytes(key)
	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	origData = PKCS5Padding(origData, blockSize)
	blockMode := cipher.NewCBCEncrypter(block, getKeyBytes(iv))
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return crypted, nil
}
func PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}
