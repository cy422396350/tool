package tool

import (
	"github.com/gin-gonic/gin"
)

/*
	type responseBodyWriter struct {
		gin.ResponseWriter
		body *bytes.Buffer
	}

	func (r responseBodyWriter) Write(b []byte) (int, error) {
		r.body.Write(b)
		return r.ResponseWriter.Write(b)
	}

	func (r responseBodyWriter) WriteString(s string) (n int, err error) {
		r.body.WriteString(s)
		return r.ResponseWriter.WriteString(s)
	}
*/
func responseBody(c *gin.Context) {
	/*w := &responseBodyWriter{body: &bytes.Buffer{}, ResponseWriter: c.Writer}
	c.Writer = w*/
	c.Next()
	for _, v := range afterhandler {
		v(c)
	}
}

var afterhandler []gin.HandlerFunc

func RegisterGinAfterHandler(c *gin.Engine, hf ...gin.HandlerFunc) {
	afterhandler = append(afterhandler, hf...)
}
