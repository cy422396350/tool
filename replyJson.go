/**
* @Auther cy
* @Date:2021/8/20 14:40
 */

package tool

import (
	"github.com/gin-gonic/gin"
	"github.com/kataras/iris/v12"
	"net/http"
)

/*
返回json数据到前台 配合showdoc使用
*/
// c context
//data 返回的json数据
func ReplyJson(c, data interface{}) {
	switch app := c.(type) {
	case *gin.Context:
		app.Set("rsp", data)
		app.JSON(http.StatusOK, data)
	case iris.Context:
		app.Values().Set("rsp", data)
		app.JSON(data)
	}
}
func ReplyCodeJson(c interface{}, code int, data interface{}) {
	switch app := c.(type) {
	case *gin.Context:
		app.Set("rsp", data)
		app.JSON(code, data)
	case iris.Context:
		app.Values().Set("rsp", data)
		app.StatusCode(code)
		app.JSON(data)
	}
}
