package tool

import (
	"context"
	"os"
	"testing"
)

func TestOss(t *testing.T) {
	var minConf MinioCos
	/*
	  secret: CChOnQ39969xju78d73bhZia1yOatfvD
	    key: IiMUZ6rnQqBl0Cgm
	    url: 159.75.92.211:9000
	    bucketname: jichengpingtaifile
	    bucket_region: xms
	*/
	minConf.BucketName = "jichengpingtaifile"
	minConf.Secret = "CChOnQ39969xju78d73bhZia1yOatfvD"
	minConf.Key = "IiMUZ6rnQqBl0Cgm"
	minConf.Url = "159.75.92.211:9000"
	minConf.BucketRegion = "xms"
	OssInit(minConf)
	ossClient := GetClient()
	open, err := os.Open("./README.md")
	if err != nil {
		t.Fatalf(err.Error())
	}
	stat, err := open.Stat()
	ossClient.PubObject(context.Background(), open, "README.md", stat.Size())
}
