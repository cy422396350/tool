package tool

import (
	"fmt"
	"testing"
	"time"
)

func TestEnableDBResolverMysql2(t *testing.T) {
	/**pgsql example*/
	/*db, err := EnableDBResolver2("pgsql",[]MysqlConf{
		{"ip:5432", "test", "postgres", "pg_pwd", "t_", 64, 16, 3600000, false},
	}, []MysqlConf{
		{"ip:5433", "test", "postgres", "pg_pwd", "t_", 64, 16, 3600000, false},
	}, nil)*/
	/** mysql example */
	db, err := EnableDBResolver2("mysql", []MysqlConf{
		{"ip:3307", "db", "root", "123456", "t_", 64, 16, 3600000, false},
	}, []MysqlConf{
		{"ip:3307", "db", "root", "123456", "t_", 64, 16, 3600000, false},
		{"ip:3308", "db", "root", "123456", "t_", 64, 16, 3600000, false},
		{"ip:3309", "db", "root", "123456", "t_", 64, 16, 3600000, false},
	}, nil)
	fmt.Println(err)
	db.AutoMigrate(&user{})
	db.Create(&user{Name: "aa"})
	time.Sleep(30 * time.Second)
	var u user
	db.First(&u)
}
func TestEnableMysql2(t *testing.T) {
	db, err := EnableMysql2(MysqlConf{
		Address:         "",
		Username:        "",
		Password:        "",
		DbName:          "",
		Prefix:          "",
		MaxOpenConns:    1,
		MaxIdleConns:    1,
		ConnMaxLifetime: 10,
	})
	fmt.Println(err)
	db.AutoMigrate(&user{})
	u := user{}
	u.Name = "test"
	db.Create(&u)
	db.Unscoped().Where("name=?", "test").Delete(&user{})
}
