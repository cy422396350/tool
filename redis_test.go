/**
* @Auther cy
* @Date:2021/3/15 16:20
 */

package tool

import (
	"fmt"
	"testing"
)

var luastr = `
local num = redis.call('GET', KEYS[1]);  

if not num then
	return 0;
else
	local res = num * ARGV[1]; 
	redis.call('SET',KEYS[1], res); 
	return res;
end`

func TestEnableRedis(t *testing.T) {
	conf := RedisConf{
		Host:        "",
		Password:    "",
		MaxIdle:     1,
		MaxActive:   30,
		IdleTimeout: 200,
	}
	EnableRedis(conf)
	/*err := RedisDB.SetString("aa", "a", 0)
	fmt.Println(err)
	str, _ := RedisDB.GetString("aa")
	fmt.Println(str)*/
	//RedisDB.Set("a",1,60)
	fmt.Println(RedisDB.LuaScript(1, []interface{}{"a", 2}, luastr))
}
func TestEnableSentinelRedis(t *testing.T) {

	conf := RedisConf{
		Host:         "",
		Password:     "密码",
		MaxIdle:      9,
		MaxActive:    30,
		IdleTimeout:  200,
		SentinelAddr: []string{"ip:16379", "ip:16380", "ip:16381"},
	}
	EnableSentinelRedis(conf)
	err := RedisDB.SetString("aa", "a", 0)
	fmt.Println(err)
	str, _ := RedisDB.GetString("aa")
	fmt.Println(str)
}
