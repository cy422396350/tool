package tool

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"hash"
	"strings"
)

// hash加密
func Hash_Encode(method string, value string) string {
	var hash hash.Hash
	switch strings.ToUpper(method) {
	case "SHA1":
		hash = sha1.New()
	case "SHA256":
		hash = sha256.New()
	default:
		hash = md5.New()
	}
	hash.Write([]byte(value))
	return hex.EncodeToString(hash.Sum(nil))
}
