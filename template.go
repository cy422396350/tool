package tool

//可通过修改模板字符串来变更模板
var ShowDocTemplateStr = "##### 简要描述\n\n- {{.title}}\n\n##### 请求URL\n- ` {{.url}} `\n\n##### 请求方式\n- {{.method}}\n\n##### 参数\n\n{{.req}}\n\n\n##### 请求示例\n\n```\n{{.reqExample}}\n```\n##### 返回示例\n\n```\n{{.respExample}}\n```\n\n##### 返回参数说明\n\n{{.resp}}\n\n\n\n##### 备注\n{{.remark}}"
var MarkdownTemplateStr = "####{{.title}}\n#####请求地址\n```text\n{{.url}}\n```\n#####请求方式\n```text\n{{.method}}\n```\n#####请求参数\n{{.req}}\n#####请求示例\n```text\n{{.reqExample}}\n```\n#####返回示例\n```text\n{{.respExample}}\n```\n#####返回参数说明\n{{.resp}}\n#####备注\n{{.remark}}"
